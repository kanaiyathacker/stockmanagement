package com.stockmanagement.service;

import java.util.List;

import com.stockmanagement.bean.ProductRule;

public interface ProductRulesService {

	List<ProductRule> getAllProductRules();
	ProductRule findProductRuleByProductCode(String productCode);
	ProductRule updateProductRule(ProductRule productRule);
}
