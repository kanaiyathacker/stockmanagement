package com.stockmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stockmanagement.bean.Order;
import com.stockmanagement.repository.OrderRepository;
import com.stockmanagement.validation.OrderValidation;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired OrderRepository orderRepository;
	@Autowired OrderValidation orderValidation;

	@Override
	public List<Order> getAllOrders() {
		return orderRepository.findAll();
	} 
	
	@Override
	public Order placeOrder(Order order) {
		orderValidation.validateOrder(order);
		order.setStatus("PENDING_ORDER");
		return orderRepository.save(order);
	}
}
