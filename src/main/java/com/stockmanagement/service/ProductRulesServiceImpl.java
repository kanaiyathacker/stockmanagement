package com.stockmanagement.service;

import java.util.List;

import javax.websocket.server.ServerEndpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stockmanagement.bean.ProductRule;
import com.stockmanagement.repository.ProductRulesRepository;

@Service
public class ProductRulesServiceImpl implements ProductRulesService{

	@Autowired private ProductRulesRepository productRulesRepository;
	
	public List<ProductRule> getAllProductRules() {
		return productRulesRepository.findAll();
	}

	public ProductRule findProductRuleByProductCode(String productCode) {
		return productRulesRepository.findProductRuleByProductCode(productCode).orElse(null);
	}

	public ProductRule updateProductRule(ProductRule productRule) {
		return productRulesRepository.save(productRule);
	}
}
