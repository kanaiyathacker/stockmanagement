package com.stockmanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stockmanagement.bean.Product;
import com.stockmanagement.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired ProductRepository productRepository;

	public List<Product> findAllProducts() {
		return productRepository.findAll();
	}

}
