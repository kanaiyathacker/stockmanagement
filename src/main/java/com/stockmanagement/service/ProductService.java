package com.stockmanagement.service;

import java.util.List;

import com.stockmanagement.bean.Product;

public interface ProductService {
	
	public List<Product> findAllProducts();
}
