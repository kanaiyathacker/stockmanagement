package com.stockmanagement.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.stockmanagement.bean.Product;
import com.stockmanagement.bean.ProductDetail;
import com.stockmanagement.bean.ProductRule;

@Service
public class StockManagementService {

	private static final String CURRENT_STOCK_IS_LESS_THAN_MINIUM_STOCK_OF_THE_PRODUCT = "Current stock is less than Minium stock of the product";
	@Autowired ProductService productService;
	@Autowired ProductRulesService productRulesService;

	
	public List<ProductDetail> checkStock() {
		List<ProductRule> allProductRules = productRulesService.getAllProductRules();
		List<Product>  allProducts = productService.findAllProducts();
		List<ProductDetail> retVal = allProducts.stream()
				   .map(p ->
				   				{
				   					ProductRule productRule = allProductRules.stream().filter(pr -> p.getProductCode().equals(pr.getProductCode())).findFirst().get();
				   					int remainingStock = p.getRemainingStock();
				   					int minimumStockLevel = productRule.getMinimumStockLevel();
				   					return new ProductDetail(p.getProductCode() , remainingStock , 
				   											 minimumStockLevel , 
				   											 remainingStock <= minimumStockLevel ? CURRENT_STOCK_IS_LESS_THAN_MINIUM_STOCK_OF_THE_PRODUCT : null);
				   				}
					 ).collect(Collectors.toList());
				
		return retVal;
	}

}
