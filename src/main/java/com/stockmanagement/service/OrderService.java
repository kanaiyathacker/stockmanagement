package com.stockmanagement.service;

import java.util.List;

import com.stockmanagement.bean.Order;

public interface OrderService {
	List<Order> getAllOrders();
	Order placeOrder(Order order);
}
