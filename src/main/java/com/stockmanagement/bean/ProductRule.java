package com.stockmanagement.bean;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class ProductRule {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private long productRuleId;
	private String productCode;
	private boolean blocked;
	private int minimumStockLevel;
	
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	public boolean isBlocked() {
		return blocked;
	}
	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public int getMinimumStockLevel() {
		return minimumStockLevel;
	}

	public void setMinimumStockLevel(int minimumStockLevel) {
		this.minimumStockLevel = minimumStockLevel;
	}

	public long getProductRuleId() {
		return productRuleId;
	}

	public void setProductRuleId(long productRuleId) {
		this.productRuleId = productRuleId;
	}
}
