package com.stockmanagement.bean;

public enum StockManagementErrors {
	PRODUCT_BLOCKED ("The product is blocked for order.");
	String description;
	
	StockManagementErrors(String description) {
		this.description = description; 
	}

	public String getDescription() {
		return description;
	}
}
