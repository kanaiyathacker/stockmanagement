package com.stockmanagement.bean;

public class ProductDetail {

	private String productCode;
	private int remainingStock;
	private int minimumStock;
	private String description;
	
	public ProductDetail(String productCode, int remainingStock, int minimumStock, String description) {
		this.productCode = productCode;
		this.remainingStock = remainingStock;
		this.minimumStock = minimumStock;
		this.description = description;
	}

	public String getProductCode() {
		return productCode;
	}

	public long getRemainingStock() {
		return remainingStock;
	}

	public long getMinimumStock() {
		return minimumStock;
	}

	public String getDescription() {
		return description;
	}
}
