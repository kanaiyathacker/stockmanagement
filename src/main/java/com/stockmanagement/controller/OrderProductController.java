package com.stockmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmanagement.bean.Order;
import com.stockmanagement.service.OrderService;

@RestController
@RequestMapping("order")
public class OrderProductController {
	
	@Autowired OrderService orderService;
	
	@GetMapping("/getAllOrders")
	public ResponseEntity<List<Order>> getAllOrders() {
		return ResponseEntity.ok(orderService.getAllOrders());
	}
	
	@PostMapping("/placeOrder")
	public ResponseEntity<Order> placeOrder(@RequestBody Order order) {
		return ResponseEntity.accepted().body(orderService.placeOrder(order));
	}
	
}
