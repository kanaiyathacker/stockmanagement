package com.stockmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmanagement.bean.ProductDetail;
import com.stockmanagement.service.StockManagementService;

@RestController
@RequestMapping("/checkStock")
public class StockManagementController {

	@Autowired StockManagementService stockManagementService;
	
	
	public ResponseEntity<List<ProductDetail>> checkStock() {
		return ResponseEntity.ok(stockManagementService.checkStock());
	}
}
