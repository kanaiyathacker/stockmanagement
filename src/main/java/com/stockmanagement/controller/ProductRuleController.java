package com.stockmanagement.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.stockmanagement.bean.ProductRule;
import com.stockmanagement.service.ProductRulesService;

@RestController
@RequestMapping("/productRules")
public class ProductRuleController {

	@Autowired ProductRulesService productRulesService;
	
	@GetMapping("/getAllProductRules")
	public ResponseEntity<List<ProductRule>> getAllProductRule() {
		return ResponseEntity.ok(productRulesService.getAllProductRules());
	}
	
	@GetMapping("/getProductRule/productCode/{productCode}")
	public ResponseEntity<ProductRule> getProductRule(@PathVariable String productCode) {
		return ResponseEntity.ok(productRulesService.findProductRuleByProductCode(productCode));
	}
	
	@PutMapping("/updateProductRule")
	public ResponseEntity<ProductRule> updateProductRule(@RequestBody ProductRule productRule) {
		return ResponseEntity.ok(productRulesService.updateProductRule(productRule));
	}
}
