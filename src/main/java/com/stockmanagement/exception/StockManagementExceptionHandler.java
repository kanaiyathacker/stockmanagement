package com.stockmanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class StockManagementExceptionHandler {
	
	@ExceptionHandler(Exception.class)
    @ResponseBody
    public ResponseEntity<com.stockmanagement.bean.Error> handleException(Exception ex) {
		ResponseEntity<com.stockmanagement.bean.Error> retVal;
		if (ex instanceof StockManagementException) {
			StockManagementException stockManagementException = (StockManagementException) ex;
			retVal = ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY).body(stockManagementException.getError());
		} else { 
			retVal = ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new com.stockmanagement.bean.Error("ERROR", ex.getMessage()));
		}
		return retVal;  
    }
}
