package com.stockmanagement.exception;

public class StockManagementException extends RuntimeException {
	private static final long serialVersionUID = 204650454323693442L;
	
	com.stockmanagement.bean.Error error;
	
	public StockManagementException() {
		super();
	}
	
	public StockManagementException(com.stockmanagement.bean.Error error) {
		super(error.getErrorDescription());
		this.error = error;
	}

	public com.stockmanagement.bean.Error getError() {
		return error;
	}
}
