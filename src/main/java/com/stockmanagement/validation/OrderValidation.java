package com.stockmanagement.validation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.stockmanagement.bean.Error;
import com.stockmanagement.bean.Order;
import com.stockmanagement.bean.ProductRule;
import static com.stockmanagement.bean.StockManagementErrors.PRODUCT_BLOCKED;
import com.stockmanagement.exception.StockManagementException;
import com.stockmanagement.service.ProductRulesService;

@Component
public class OrderValidation {

	@Autowired ProductRulesService productRulesService; 
	
	public void validateOrder(Order order) {
		ProductRule productRuleByProductCode = productRulesService.findProductRuleByProductCode(order.getProductCode());
		if(productRuleByProductCode.isBlocked())
			throw new StockManagementException(new Error(PRODUCT_BLOCKED.name() , PRODUCT_BLOCKED.getDescription()));
		
	}
}
